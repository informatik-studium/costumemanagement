package org.iu.torbenwetter.costumemanagement;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CostumeRepository extends CrudRepository<Costume, Long> {
}
