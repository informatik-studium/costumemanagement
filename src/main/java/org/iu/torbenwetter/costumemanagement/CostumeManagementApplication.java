package org.iu.torbenwetter.costumemanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CostumeManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(CostumeManagementApplication.class, args);
    }

}
