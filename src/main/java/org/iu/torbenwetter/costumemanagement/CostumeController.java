package org.iu.torbenwetter.costumemanagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CostumeController {

    private final CostumeRepository costumeRepository;

    @Autowired
    public CostumeController(CostumeRepository costumeRepository) {
        this.costumeRepository = costumeRepository;
    }

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("costumes", this.costumeRepository.findAll());
        return "index";
    }

    @GetMapping("/addCostumeForm")
    public String addCostumeForm(Model model) {
        model.addAttribute("costume", new Costume());
        return "createForm";
    }

    @PostMapping("/saveCostume")
    public String saveCostume(@ModelAttribute Costume costume) {
        this.costumeRepository.save(costume);
        return "redirect:/";
    }
}
